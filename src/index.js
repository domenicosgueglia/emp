import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import Homepage from './Components/Homepage/Homepage';
import * as serviceWorker from './serviceWorker';
import 'semantic-ui-css/semantic.min.css'
import {
    Route,
    Switch,
    Redirect,
    Router
} from "react-router-dom";
import Login from './Components/Login/Login';
import configureStore from './redux/store'
import { Provider } from 'react-redux'
import LoginHomePage from './Components/LoginHomePage/LoginHomePage';
import history from './history'

const store = configureStore();


ReactDOM.render(
    <Provider store={store}>
        <Router history={history} >
            <Switch>
                <Route path='/login'>
                    <Login />
                </Route>
                <Route exact path="/">
                    <Homepage />
                </Route>
                <PrivateRoute path="/userInfo">
                    <LoginHomePage />
                </PrivateRoute>
            </Switch>

        </Router></Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

function PrivateRoute({ children, ...rest }) {
    return (
        <Route
            {...rest}
            render={({ location }) =>
                sessionStorage.id ? (
                    children
                ) : (
                        <Redirect
                            to={{
                                pathname: "/login",
                                state: { from: location }
                            }}
                        />
                    )
            }
        />
    );
}