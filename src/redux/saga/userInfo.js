import { call, take,put} from 'redux-saga/effects'
import userInfo from './../services/userinfo'
import * as types from './../constants/actions'

 export default function* getUserInfo() {
    while (true) {
        try {
            yield take(types.GET_USER_INFO)
            const id=sessionStorage.getItem('id')
            let response = yield call(userInfo.getUserInfo,id)
             yield put({type:types.UPDATE_USER_REDUCER,payload:response})
        } catch (e) {
            console.log(e)
        }
    }
}

