import { call, take } from 'redux-saga/effects'
import auth from './../services/auth'
import * as types from './../constants/actions'
import history  from '../../history';

export default function* login() {
    while (true) {
        try {
            yield take(types.LOGIN)
            let response = yield call(auth.login)
            sessionStorage.setItem('id', response.id)
            history.push('/userInfo')
            
        } catch (e) {
            console.log(e)
        }
    }
}

