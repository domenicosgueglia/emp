import { call, take, put } from 'redux-saga/effects'
import userInfo from './../services/orders'
import * as types from './../constants/actions'

export default function* getOrders() {
    while (true) {
        try {
            yield take(types.GET_ORDERS)
            const id=sessionStorage.getItem('id')
            let response = yield call(userInfo.getUserOrders,id)
            yield put({ type: types.UPDATE_ORDERS_REDUCER, payload: response })
        } catch (e) {
            console.log(e)
        }
    }
}

