import userInfo from './userInfo'
import orders from './orders'
import auth from './auth'
import {all } from 'redux-saga/effects'


export default function* rootSaga() {
    yield all([
        auth(),
        userInfo(),
        orders()
    ]);
}
