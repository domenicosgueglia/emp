import { combineReducers } from 'redux'
import userInfo from './userInfo'
import orders  from './orders'

export default combineReducers({
userInfo,
orders
})
