export default function reducer(state = {}, action) {
    switch (action.type) {
        case 'UPDATE_USER_REDUCER':
            return { ...state, ...action.payload }
        default:
            return state;
    }
   
}