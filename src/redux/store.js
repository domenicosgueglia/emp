import rootReducer from './reducers/root'
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga'
import rootSaga from './saga/root'
import logger from 'redux-logger'

export default function configureStore() {
    const sagaMiddleware = createSagaMiddleware();
    const store = createStore(rootReducer, applyMiddleware(sagaMiddleware,logger));
    sagaMiddleware.run(rootSaga)
    return store;
}