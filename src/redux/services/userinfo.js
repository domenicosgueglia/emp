
import axios from 'axios'

let request = {
    getUserInfo(id) {
       
        return axios.get(`/users/${id}`).then(r => r.data)
    }

}
export default request;


