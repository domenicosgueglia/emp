
import axios from 'axios'

let request = {
    login() {
        return axios.post(`/login`).then(r => r.data)
    }
}
export default request;