import axios from 'axios'


let request = {
    getUserOrders(id) {
        return axios.get(`/users/${id}/orders`).then(r => r.data)
    },
    deleteOrder(orderId){
        return axios.delete(`/orders/${orderId}`).then(r => r.data)
    }

}
export default request;