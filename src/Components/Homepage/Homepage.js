import React from 'react';
import Header from './../Header/Header.js'
import Body from './../Body/Body.js'
import Navbar from './../Navbar/Navbar.js'
import Footer from '../Footer/Footer.js';


class Homepage extends React.Component {
  render() {
    return (
      <div>
        <Navbar />
        <Header />
        <Body />
        <Footer/>
      </div>
    );
  }
}

export default Homepage;

