import React from 'react'
import './Orders.scss'
import { useSelector, shallowEqual } from 'react-redux'
import { Card, Modal, Header } from 'semantic-ui-react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTruck } from '@fortawesome/free-solid-svg-icons'


//renders all the orders that it gets from the backend. each order is wrapped in a Modal that will be shown when the order will be clicked
function Orders() {
    const { orders } = useSelector(state => state.orders, shallowEqual)

    return <Card.Group>
            {orders.map((order) => renderOrders(order))}
        </Card.Group>
    

}

//create a card for each item in the order and wrap it into a modal
function renderOrders(order) {
      
    return order.items.map((item, index) =>
        <Modal key={index}  trigger={
            <Card className='card_properties' link={true} color={order.status === 'paid' ? 'green' : 'red'}  >
                <Card.Content>
                    <Card.Header>
                        {item.name}
                    </Card.Header>
                    <Card.Meta>
                        <span> {order.status}: USD {item.amount}$</span>
                    </Card.Meta>
                    <Card.Description>
                        Open the order for more informations
                    </Card.Description>
                </Card.Content>
                {order.tracking && <Card.Content extra>
                    <FontAwesomeIcon icon={faTruck} />
                </Card.Content>}
            </Card>}>
            <Modal.Header>{item.name}</Modal.Header>
            <Modal.Content>
                <Modal.Description>
                    <Header>Ref:{order.ref}</Header>
                    {order.discounts.map((discount,index)=><p key={index}>Discount: {discount.name}</p>)}
                    {order.tracking && <div><p>Carrier: {order.tracking.carrier}</p>
                        <p>Status: {order.tracking.status}</p>
                        <p>Tracking Code: {order.tracking.trackingCode}</p>
                    </div>}
                </Modal.Description>
            </Modal.Content>
        </Modal>)
}



export default Orders