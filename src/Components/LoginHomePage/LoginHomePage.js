import React from 'react'
import Navbar from '../Navbar/Navbar'
import Sidebar from '../Sidebar/Siderbar'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser, faList } from '@fortawesome/free-solid-svg-icons'
import * as types from '../../redux/constants/actions'

//based on how many elements we have the sidebar will render new tabs
class LoginHomePage extends React.Component {

    render() {
        const elements = [
            { action: types.GET_USER_INFO, name: 'User Profile', icon: <FontAwesomeIcon icon={faUser}></FontAwesomeIcon> },
            { action: types.GET_ORDERS, name: 'Orders', icon: <FontAwesomeIcon icon={faList}></FontAwesomeIcon> }]

        return <div style={{ height: '100%' }}>
            <Navbar isUserInfo={true} />
            <Sidebar elements={elements}></Sidebar>
            
        </div>
    }
}

export default LoginHomePage