import React from 'react'
import './Navbar.scss'
import { Button } from 'semantic-ui-react'
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons'

class Navbar extends React.Component {
    render() {
        return <div>
            {this.props.isUserInfo ? <div className='navbar' style={{position:'relative'}}>
                
                <img className='sidebar_logo_properties' src="https://www.empatica.com/assets/images/logo/empatica_logo_red.svg" alt='empatica_logo'></img>
                <Link to='/' onClick={()=>sessionStorage.removeItem('id')}>
                    <FontAwesomeIcon title='Logout' icon={faSignOutAlt} className='logout_button' >
                    </FontAwesomeIcon>
                </Link>
            </div>
                :
                <div className='navbar'>
                    <img className='logo_properties' src="https://www.empatica.com/assets/images/logo/empatica_logo_red.svg" alt='empatica_logo'></img>
                    <Link to='/login'>
                        <Button className='login_button'>Login</Button>
                    </Link>
                </div>
            }
        </div>
    }
}

export default Navbar