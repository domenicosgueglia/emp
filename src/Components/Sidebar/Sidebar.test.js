import React from 'react';
import Sidebar from './Siderbar.js';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { mount } from "enzyme";
import { createStore } from 'redux';
import reducer from './../../redux/reducers/root'
import { Provider } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser, faList } from '@fortawesome/free-solid-svg-icons'
import * as types from '../../redux/constants/actions'
import UserInfo from '../UserInfo/UserInfo.js';
configure({ adapter: new Adapter() });



it('renders user info first', () => {
  const elements = [
    { action: types.GET_USER_INFO, name: 'User Profile', icon: <FontAwesomeIcon icon={faUser}></FontAwesomeIcon> },
    { action: types.GET_ORDERS, name: 'Orders', icon: <FontAwesomeIcon icon={faList}></FontAwesomeIcon> }]
  let orders = {

  }
  const mockStore = createStore(reducer, orders);
  const wrapper = mount(<Provider store={mockStore}><Sidebar elements={elements} /></Provider>);
  expect(wrapper.find(UserInfo).length).toEqual(1);
});

