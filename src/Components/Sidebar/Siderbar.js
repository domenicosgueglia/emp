import React, { useEffect,useState } from 'react'
import './Sidebar.scss'
import { useDispatch } from 'react-redux'
import UserInfo from '../UserInfo/UserInfo'
import Orders from '../Orders/Orders'


//the sidebar  will dispatch  on mount 2 actions to fetch data from the backend. The first component to be rendered
//is the UserInfo. The renderComponent function will render a different component when the state of the function will change
function Sidebar(props) {
    const dispatch = useDispatch()
    const [componentToRender, setComponentToRender] = useState('User')
    useEffect(() => { dispatch({ type: props.elements[0].action }); dispatch({ type: props.elements[1].action }) },[])

    return (
        <div className='wrapper'>
            <div className="sidebar">
                {props.elements.map((object, i) =>
                    <div key={i} onClick={() => { setComponentToRender(object.name) }}
                        className='sidebar_element'><span className='icon_position'>{object.icon}</span>{object.name}</div>
                )
                }
            </div>
            {renderComponent(componentToRender)}

        </div>
    )

}

function renderComponent(componentToRender) {
    switch (componentToRender) {
        case 'User':
            return <UserInfo />
        case 'Orders':
            return <Orders/>
        default:
            return <UserInfo />
    }
}

export default Sidebar