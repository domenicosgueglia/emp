import React from 'react';
import { Button } from 'semantic-ui-react'
import './Body.scss';
import embrace_logo from './../../assets/images/embrace_logo.svg'
import embrace_logo_sm from './../../assets/images/embrace_logo-sm.svg'
import embrace_front_sm from './../../assets/images/embrace_front-hdpi.jpg'
import embrace_front from './../../assets/images/embrace_front-xhdpi.jpg'


class Body extends React.Component {
  render() {
    return (
    
        <div className='wrapper_properties' >
          <picture>
            <source media="(min-width: 700px)" srcSet={embrace_logo}></source>
            <img src={embrace_logo_sm} alt='embrace_logo' />
          </picture>
          <div className='body_properties'>
          <div className="subtitle_properties ">Your companion for epilepsy management</div>
          <div className="paragraph_properties">The Embrace watch uses advanced learning algoritmhs to identify tonic-clonic seizures and send alerts to caregivers.
           It also provides sleep,rest,and physical activity analisis.</div>
          <div className="front_properties">
            <picture>
              <source media="(min-width: 700px)" srcSet={embrace_front}></source>
              <img src={embrace_front_sm} alt='embrace_logo' />
            </picture>
          </div>
          </div>
          <Button >Discover Embrace Features</Button>
        </div>
        

    );
  }
}

export default Body;

