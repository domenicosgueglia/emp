import React from 'react';
import { Button } from 'semantic-ui-react'
import './Header.scss';
import homepage_hero from './../../assets/images/homepage_hero-lg-xhdpi.jpg'
import homepage_hero_sm from './../../assets/images/homepage_hero-sm-xhdpi.jpg'

class Header extends React.Component {
  render() {
    return (
      
        <div>
          <div className="text_container">
            <div className="centered">Smart technology for people living with epilepsy</div>
            <Button inverted className="button_properties">Buy Now</Button>
            <div className="info_properties">Comes with a 30-day Free Trial Subscription</div>
          </div>
          <picture>
            <source media="(min-width: 700px)" srcSet={homepage_hero}></source>
            <img src={homepage_hero_sm} alt='embrace' width='100%' />
          </picture>
        </div>

   

    );
  }
}

export default Header;

