import React from 'react'
import './UserInfo.scss'
import { useSelector, shallowEqual } from 'react-redux'


const info_values = {
    firstName: 'Name',
    lastName: 'Last Name',
    email: 'Email'
}

//UserInfo shows different informations based on what we get from the backend
function UserInfo() {
    const userInfo = useSelector(state => state.userInfo, shallowEqual)

    function renderInfo() {
        let infos = []
        for (let info of Object.entries(userInfo)) {
            if (info[0] !== 'id') {
                infos.push(<div key={info[0]} className='user_info_properties'>
                    <span>{info_values[info[0]]}</span>
                    <span>{info[1]}</span>
                    <div className='separator'></div>
                </div>)
            }
        }
        return infos
    }
    return <div>
        {renderInfo()}
    </div>

}

export default UserInfo