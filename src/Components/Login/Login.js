import React, { useState } from 'react'
import './Login.scss'
import { useDispatch } from 'react-redux'
import { Form, Button, Message } from 'semantic-ui-react'
import history from './../../history'
import * as types from '../../redux/constants/actions'

function Login() {
    const dispatch = useDispatch()
    const [email, setEmail] = useState('')
    const [isValidEmail, setValidEmail] = useState(email === 'john@doe.com')

    return <div>
        <img className='img_logo' src="https://www.empatica.com/assets/images/logo/empatica_logo_red.svg" alt='empatica_logo'></img>
        <div className='login_form'>
            <Form className="form_properties" error>
                <Form.Field>
                    <label>Email</label>
                    {/*the only valid email is john@doe.com  */}
                    <input placeholder='Email' onBlur={(e) => { setEmail(e.target.value); setValidEmail(e.target.value === 'john@doe.com') }} />
                     {/*if the email is invalid an error will be prompted */}
                    {!isValidEmail && email !== '' && <Message
                        error
                        className='error_message'
                        header='Action Forbidden'
                        content='Invalid email'
                    />}
                </Form.Field>

            </Form>
              {/*the login action will be dispatched only if the email is a valid one */}
            <Button className='continue_button' onClick={() => { isValidEmail && dispatch({ type: types.LOGIN }) }}>Continue</Button>
            <Button className='continue_button' onClick={() =>history.goBack()}>Go Back</Button>
        </div>
    </div>

}

export default Login